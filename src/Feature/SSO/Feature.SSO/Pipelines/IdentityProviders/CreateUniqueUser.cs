﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Sitecore.Diagnostics;
using Sitecore.Owin.Authentication.Configuration;
using Sitecore.Owin.Authentication.Identity;
using Sitecore.Owin.Authentication.Services;
using Sitecore.SecurityModel.Cryptography;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace Feature.SSO.Pipelines.IdentityProviders
{
    public class CreateUniqueUser : DefaultExternalUserBuilder
    {
        private readonly IHashEncryption _hashEncryption;

        public CreateUniqueUser(ApplicationUserFactory applicationUserFactory, IHashEncryption hashEncryption) : base(applicationUserFactory, hashEncryption)
        {
            _hashEncryption = hashEncryption;
        }

        [SuppressMessage("Microsoft.Naming", "CA1726:UsePreferredTerms", MessageId = "Login")]
        protected override string CreateUniqueUserName(UserManager<ApplicationUser> userManager, ExternalLoginInfo externalLoginInfo)
        {
            Assert.ArgumentNotNull((object)userManager, nameof(userManager));
            Assert.ArgumentNotNull((object)externalLoginInfo, nameof(externalLoginInfo));
            IdentityProvider identityProvider = this.FederatedAuthenticationConfiguration.GetIdentityProvider(externalLoginInfo.ExternalIdentity);
            if (identityProvider == null)
                throw new InvalidOperationException("Unable to retrieve identity provider for given identity");
            var domain = identityProvider.Domain;
            string userName;
            do
            {
                userName = domain + "\\" + externalLoginInfo.Email;
            }
            while (userManager.FindByName<ApplicationUser, string>(userName) != null);
            return userName;
        }
    }
}