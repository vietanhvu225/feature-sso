﻿namespace Feature.SSO.Pipelines.IdentityProviders
{
    using System.Threading.Tasks;
    using Microsoft.Owin.Infrastructure;
    using Owin;
    using Sitecore.Abstractions;
    using Sitecore.Diagnostics;
    using Sitecore.Owin.Authentication.Configuration;
    using Sitecore.Owin.Authentication.Extensions;
    using Sitecore.Owin.Authentication.Pipelines.IdentityProviders;
    using Sitecore.Owin.Authentication.Services;

    public class Facebook : IdentityProvidersProcessor
    {
        protected override string IdentityProviderName => "Facebook";

        protected IdentityProvider IdentityProvider { get; set; }

        public Facebook(FederatedAuthenticationConfiguration federatedAuthenticationConfiguration, ICookieManager cookieManager, BaseSettings baseSettings) : base(federatedAuthenticationConfiguration, cookieManager, baseSettings)
        {
        
        }

        protected override void ProcessCore(IdentityProvidersArgs args)
        {
            Assert.ArgumentNotNull(args, "args");
            IdentityProvider = this.GetIdentityProvider();

            var provider = new Microsoft.Owin.Security.Facebook.FacebookAuthenticationProvider
            {
                OnAuthenticated = (context) =>
                {
                    //map claims
                    context.Identity.ApplyClaimsTransformations(new TransformationContext(this.FederatedAuthenticationConfiguration, IdentityProvider));
                    return Task.CompletedTask;
                },
                OnReturnEndpoint = (context) =>
                {
                    return Task.CompletedTask;
                },
            };

            var fbAuthOptions = new Microsoft.Owin.Security.Facebook.FacebookAuthenticationOptions
            {
                AppId = Settings.GetSetting("Sitecore.Feature.Accounts.Facebook.AppId"),
                AppSecret = Settings.GetSetting("Sitecore.Feature.Accounts.Facebook.AppSecret"),
                Provider = provider,
                AuthenticationType = IdentityProvider.Name
            };

            args.App.UseFacebookAuthentication(fbAuthOptions);
        }
    }
}